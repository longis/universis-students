import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {EventsModule} from '@universis/ngx-events';
import {CommonModule} from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {SharedModule} from '@universis/common';
import {MostModule} from '@themost/angular';
import {TeachingEventComponent} from './components/teaching-event/teaching-event.component';
import {TeachingEventRoutingModule} from './teaching-event-routing.module';

import * as el from "./i18n/teaching-events.el.json"
import * as en from "./i18n/teaching-events.en.json"


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    MostModule,
    TeachingEventRoutingModule,
    EventsModule
  ],
  declarations: [
    TeachingEventComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TeachingEventsModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}
