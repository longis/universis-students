import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ComponentLoaderFactory} from 'ngx-bootstrap/component-loader';
import {PositioningService} from 'ngx-bootstrap/positioning';
import { TooltipConfig, TooltipModule} from 'ngx-bootstrap/tooltip';

import {ProfileService} from 'src/app/profile/services/profile.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import { ConfigurationService } from '@universis/common';
import {TranslateModule} from '@ngx-translate/core';

import {ProgressBarDegreeComponent} from './progress-bar-degree.component';
import {GradesService, GradeAverageResult} from 'src/app/grades/services/grades.service';
import { GradeScaleService, GradeScale } from '@universis/common';
import {TestingConfigurationService} from '../../../test';

describe('ProgressBarDegreeComponent', () => {
    let component: ProgressBarDegreeComponent;
    let fixture: ComponentFixture<ProgressBarDegreeComponent>;

    const profileSvc = jasmine.createSpyObj('ProfileService', ['getStudent']);
    const gradeSvc = jasmine.createSpyObj('GradesService', ['getAllGrades', 'getCourseTeachers', 'getGradeInfo',
                                          'getDefaultGradeScale', 'getThesisInfo', 'getLastExamPeriod',
                                          'getRecentGrades', 'getGradesSimpleAverage', 'getGradesWeightedAverage']);
    const gradeScaleSvc = jasmine.createSpyObj('GradeScaleService', ['getGradeScales', 'getGradeScale']);

    profileSvc.getStudent.and.returnValue(Promise.resolve(JSON.parse('{"studyProgram":{"semesters":2},"studentStatus":{"alternateName":"active"}, "graduationGradeScale": 1234, "graduationGrade": 0.88}')));
    gradeSvc.getGradeInfo.and.returnValue(Promise.resolve(JSON.parse('[]')));
    const gradeScale = Object.assign(new GradeScale('en'), JSON.parse('{"formatPrecision":2, "scaleType":0, "scaleFactor":1}'));
    gradeSvc.getDefaultGradeScale.and.returnValue(Promise.resolve(gradeScale));
    gradeScaleSvc.getGradeScale.and.returnValue('{"formatPrecision":2, "scaleType":0, "scaleFactor":1}');
    const grades: GradeAverageResult = {
      average: 0,
      passed: 0,
      coefficients: 0,
      grades: 0,
      courses: 0,
      ects: 0,
      units: 0
    };
    gradeSvc.getGradesSimpleAverage.and.returnValue(grades);
    gradeSvc.getGradesWeightedAverage.and.returnValue(grades);

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [ProgressBarDegreeComponent],
            providers: [
              ComponentLoaderFactory,
              PositioningService,
              TooltipConfig,
              {
                  provide: ProfileService,
                  useValue: profileSvc
              },
              {
                  provide: GradesService,
                  useValue: gradeSvc
              },
              {
                  provide: GradeScaleService,
                  useValue: gradeScaleSvc
              },
              {
                  provide: ConfigurationService,
                  useClass: TestingConfigurationService
              }
            ],
            imports: [TooltipModule,
                HttpClientTestingModule,
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                })]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProgressBarDegreeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
