import { Component } from '@angular/core';
import {ProfileService} from '../profile/services/profile.service';
import { DiagnosticsService } from '@universis/common';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss'],
  providers: [ProfileService]
})
export class DashboardComponent {
  public student: any;
  public diningServiceEnabled: boolean = false;
  /*
     if is true it will shown component progress-bar-semester
     if is false it will shown component progress-bar-degree
    */
  public viewsemester = false;

  constructor(private _profileService: ProfileService,
              private _diagnostics: DiagnosticsService) {
    this._profileService.getStudent().then(res => {
      this.student = res; // Load data
    });
    // check if dining service is enabled
    // do not await this call
    this._diagnostics.hasService('DiningService').then((hasDiningService: boolean) => {
      this.diningServiceEnabled = hasDiningService;
    }).catch(err => {
      // just log the error
      console.error(err);
    })
  }

}
