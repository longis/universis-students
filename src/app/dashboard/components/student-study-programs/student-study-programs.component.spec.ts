import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentStudyProgramsComponent } from './student-study-programs.component';

describe('StudentStudyProgramsComponent', () => {
  let component: StudentStudyProgramsComponent;
  let fixture: ComponentFixture<StudentStudyProgramsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentStudyProgramsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StudentStudyProgramsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
