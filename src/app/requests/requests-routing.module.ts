import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '@universis/common';
import {RequestsHomeComponent} from './components/requests-home/requests-home.component';
import {RequestsListComponent} from './components/requests-list/requests-list.component';
import {RequestsNewComponent} from './components/requests-new/requests-new.component';

const routes: Routes = [

  {
    path: '',
    component: RequestsHomeComponent,
    canActivate: [
      AuthGuard
    ],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: RequestsListComponent
      },
      {
        path: 'new',
        component: RequestsNewComponent
      },
      {
        path: 'ExamPeriodParticipateActions',
        // tslint:disable-next-line:max-line-length
        loadChildren: () => import('../exam-period-participate-action/exam-period-participate-action.module').then(m => m.ExamPeriodParticipateActionModule)
      },
      {
        path: 'StudentRemoveActions',
        loadChildren: () => import('../removal-request-action/removal-request-action.module').then(m => m.RemovalRequestActionModule)
      },
      {
        path: 'RequestSuspendActions',
        loadChildren: () => import('../suspend-request-action/suspend-request-action.module').then(m => m.SuspendRequestActionModule)
      },
      {
        path: 'ThesisRequestActions',
        loadChildren: () => import('../thesis-request-action/thesis-request-action.module').then(m => m.ThesisRequestActionModule)
      },
      {
        path: 'StudentGradeRemarkActions',
        // tslint:disable-next-line:max-line-length
        loadChildren: () => import('../student-grade-remark-action/student-grade-remark-action.module').then(m => m.StudentGradeRemarkActionModule)
      },
      {
        path: 'PreferredSpecialtyRequestActions',
        // tslint:disable-next-line:max-line-length
        loadChildren: () => import('../preferred-specialty-request-action/preferred-specialty-request-action.module').then(m => m.PreferredSpecialtyRequestActionModule)
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestsRoutingModule { }
