import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { template, at } from 'lodash';
import { ProfileService } from '../../../profile/services/profile.service';
import { CurrentRegistrationService } from '../../../registrations/services/currentRegistrationService.service';
import { ConfigurationService } from '@universis/common';
import { LinkProfile, NgxProfilesService } from '@universis/ngx-profiles';

@Component({
  selector: 'app-student-study-programs',
  templateUrl: './student-study-programs.component.html',
  styleUrls: ['./student-study-programs.component.scss']
})
export class StudentStudyProgramsComponent implements OnInit {

  public studyPrograms: any = [];
  public currentRegistration: any;
  public isLoading = false;   // Only if data is loaded
  public currentRegistrarionEffectiveStatus: any;
  public defaultLanguage: string | any = "";
  public currentLanguage: string = "";
  public timetableExists = false;

  public registrationEdited = false;

    public linkProfiles?: LinkProfile[];

  constructor(private _context: AngularDataContext, private _profileService: ProfileService,
    private _configurationService: ConfigurationService,
      private _profiles: NgxProfilesService) {
    this.currentLanguage = this._configurationService.currentLocale;
    this.defaultLanguage = this._configurationService?.settings?.i18n?.defaultLocale;
  }

  ngOnInit() {
    this.isLoading = true;
    this.linkProfiles = [];

    this._profileService.getStudent().then(async student => {
      this.registrationEdited = sessionStorage['registrationEdited'];
      this._context.model('TimetableEvents')
        .where('organizer').equal(student.department.id)
        .and('academicYear').equal(student.department.currentYear.id)
        .and('academicPeriods/id').equal(student.department.currentPeriod.id)
        .and('availableEventTypes/alternateName').equal('TeachingEvent')
        .and('eventStatus/alternateName').equal('EventOpened')
        .getItem()
        .then(timetable => this.timetableExists = !!timetable)

      //  get current registration's effective status
      // this._currentReg.getCurrentRegistrationEffectiveStatus().then(effectiveStatus => {
      //   if (effectiveStatus) {
      //     this.currentRegistrarionEffectiveStatus = effectiveStatus.status;
      //   }
      //   this._currentReg.getCurrentRegistration().then((currentReg: any = {}) => {
      //     if (currentReg == null) {
      //       this.currentRegistration = {
      //         student: student.id,
      //         registrationYear: student.department.currentYear,
      //         registrationPeriod: student.department.currentPeriod,
      //         classes: []
      //       };
      //     }
      //     if (currentReg && currentReg.classes) {
      //       this.generateClassAndELearningUrls(currentReg.classes).then((res) => {
      //         this.studyPrograms = res;
      //         this.studyPrograms.sort((a, b) => a.courseClass.course.name.localeCompare(b.courseClass.course.name));
      //         this.currentRegistration = currentReg;
      //       });
      //     }
      //     this.isLoading = false; // Data is loaded
      //   }).catch(err => {
      //     if (err.error.statusCode === 404) {
      //       this.currentRegistration = {
      //         student: student.id,
      //         registrationYear: student.department.currentYear,
      //         registrationPeriod: student.department.currentPeriod,
      //         classes: []
      //       };
      //       this.isLoading = false;
      //     }
      //   });
      // });
          // get Student configuration
          const student1 = await this._profileService.getStudent();
          if (student) {
            // get use register action flag
            // this.useStudentRegisterAction = student &&
            //   student.department &&
            //   student.department.organization &&
            //   student.department.organization.instituteConfiguration &&
            //   student.department.organization.instituteConfiguration.useStudentRegisterAction;
            // if institute does not use student register action
            // if (!this.useStudentRegisterAction) {
            //   // find registrations menu
            //   // const findItem = this._appSidebar.navigationItems.find(x => {
            //   //   return x.url === '/registrations';
            //   // });
            //   // if (findItem) {
            //   //   // try to find register semester action
            //   //   const findIndex = findItem.children.findIndex(x => {
            //   //     return x.url === '/registrations/semester';
            //   //   });
            //   //   // if register semester exists, remove it
            //   //   if (findIndex >= 0) {
            //   //     findItem.children.removeItem.splice(findIndex, 1);
            //   //   }
            //   // }
            // }
            // build languages
            // this.buildLanguages(this._router.url.toString());
            // // set navigation items
            // this.navItems = this._appSidebar.navigationItems;
            // for (let i = 0; i < this.languages.length; i++) {
            //   this.languages[i] = ISO6391.getNativeName(this.languages[i]);
            //   this._translateService.onLangChange.subscribe((event: LangChangeEvent) => {
            //     window.location.reload();
            //   });
            // }
          }
          this._profiles.supported().then((supportsProfiles) => {
            if (supportsProfiles) {
              this._profiles.getStudentProfiles().then((linkProfiles) => {
                linkProfiles.forEach((item) => {
                  item.rel = item.profile === `urn:universis:student:profile:${student.id}` ? 'canonical' : 'alternate';
                });
                this.linkProfiles = linkProfiles;
              });
            }
          });

    });
    this.isLoading = false;
  }
  goToLink(url: string) {
    window.open(url, '_blank');
  }

  async generateClassAndELearningUrls(courses: Array<any>) {
    const student = await this._profileService.getStudent();
    if (student && student.department && student.department.organization && student.department.organization.instituteConfiguration) {
      const instituteConfig = student.department.organization.instituteConfiguration;
      courses.map(course => {
        course.courseClass.classUrl =
          instituteConfig.courseClassUrlTemplate ? template(instituteConfig.courseClassUrlTemplate)(course.courseClass) : '';
        course.courseClass.eLearningUrl =
          instituteConfig.eLearningUrlTemplate ? template(instituteConfig.eLearningUrlTemplate)(course.courseClass) : '';
      });
    }
    return courses;
  }
}
