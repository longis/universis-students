import { Component, OnInit, ViewChild } from '@angular/core';
import { AppSidebarService } from '../students-shared/services/app-sidebar.service';
import { UserService, DiagnosticsService, ApiServerStatus, removeMemoizeKey } from '@universis/common';
import { ConfigurationService } from '@universis/common';
import { NavigationEnd, Router } from '@angular/router';
import { ProfileService } from '../profile/services/profile.service';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { MessagesService } from '../messages/services/messages.service';
import { ApplicationSettings, INavigationLinkConfiguration } from '../students-shared/students-shared.module';
import * as ISO6391 from 'iso-639-1/build';
import { MessageSharedService } from '../students-shared/services/messages.service';
import { ToastrService } from 'ngx-toastr';
import { GradesService } from './../grades/services/grades.service';
import { environment } from '../../environments/environment';
import { LinkProfile, NgxProfilesService } from '@universis/ngx-profiles';

import * as el from "../messages/i18n/messages.el.json"
import * as en from "../messages/i18n/messages.en.json"

@Component({
  selector: 'app-layouts',
  templateUrl: './full-layout.component.html',
  styles: [`
    .inner__content {
      max-height: 70vh;
      overflow-y: auto;
    }
    button.dropdown-item {
      cursor: pointer;
    }
    `],
  providers: [MessagesService]
})

export class FullLayoutComponent implements OnInit {
  public useStudentRegisterAction = false;

  constructor(private _appSidebar: AppSidebarService,
    private _configurationService: ConfigurationService,
    private _userService: UserService,
    private notificationsMsgService: MessagesService,
    private _router: Router,
    private _profileService: ProfileService,
    private _translateService: TranslateService,
    private messageSharedService: MessageSharedService,
    private _diagnosticsService: DiagnosticsService,
    private _toastrService: ToastrService,
    private _profiles: NgxProfilesService) {
    this._router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.buildLanguages(event.urlAfterRedirects.toString());
        this.navItems = this._appSidebar.navigationItems;
      }
    });

    // called from message list component
    this.messageSharedService.getAction().subscribe(async message => {
      this.numOfMsg = await this.notificationsMsgService.getCountOfUnreadMessages();
    });
  }

  @ViewChild('appSidebarNav') appSidebarNav: any;
  public status = { isOpen: false };
  public user;
  public currentLang;
  public curLang;
  public languages: any = [];
  public lang: any = {};
  public navItems: any = [];
  public logoPath;
  public header: any;
  public messages: any;
  public unreadmsg: any;
  public numOfMsg = [
    {
      total: 0
    }
  ];
  public take = 4;
  public skip = 0;
  public navigationLinks;
  public waitForMessages = false;
  public dbstatus?: ApiServerStatus;
  public linkProfiles?: LinkProfile[];

  async ngOnInit() {
    this.linkProfiles = [];
    // get current user
    this.user = await this._userService.getUser();
    // get current language
    this.currentLang = ISO6391.getNativeName(this._configurationService.currentLocale);
    // get current language's first two letters
    this.curLang = this.currentLang.slice(0, 2);
    // get brand name
    const headers = this._configurationService.settings.app && (<ApplicationSettings>this._configurationService.settings.app).header;
    if (Array.isArray(headers)) {
      const header = headers.find(x => {
        return x.additionalType === 'Header' && x.inLanguage === this._configurationService.currentLocale;
      });
      if (header && header.body && header.body.trim().length) {
        this.header = header;
      }
    }
    // get languages
    this.languages = this._configurationService?.settings?.i18n?.locales;
    // get count of messages
    this.numOfMsg = await this.notificationsMsgService.getCountOfUnreadMessages();
    // get path of brand logo
    this.logoPath = this._configurationService?.settings?.app?.image;
    // get Navigation Links Collection for current Language
    this.navigationLinks = <INavigationLinkConfiguration>((<any>this._configurationService.settings.app).navigationLinks || []).filter(x => {
      return x.inLanguage === this._translateService.currentLang;
    });
    // get database status
    this.dbstatus = await this._diagnosticsService.getStatus();
    const paths = ['#/registrations', '#/registrations/semester', '#/registrations/courses', '#/registrations/list','#/grades/recent', '#/grades/theses', '#/graduation']
    paths.forEach(path => {
      const findItems = document.querySelectorAll("a[href='"+path+"");
      findItems.forEach(item => {
        item.remove();
      });
    })
    // get Student configuration
    const student = await this._profileService.getStudent();
    if (student) {
      // get use register action flag
      this.useStudentRegisterAction = student &&
        student.department &&
        student.department.organization &&
        student.department.organization.instituteConfiguration &&
        student.department.organization.instituteConfiguration.useStudentRegisterAction;
      // if institute does not use student register action
      if (!this.useStudentRegisterAction) {
        // find registrations menu
        const findItem = this._appSidebar.navigationItems.find(x => {
          return x.url === '/registrations';
        });
        // if (findItem) {
        //   // try to find register semester action
        //   const findIndex = findItem.children.findIndex(x => {
        //     return x.url === '/registrations/semester';
        //   });
        //   // if register semester exists, remove it
        //   if (findIndex >= 0) {
        //     findItem.children.removeItem.splice(findIndex, 1);
        //   }
        // }
      }
      // build languages
      this.buildLanguages(this._router.url.toString());
      // set navigation items
      this.navItems = this._appSidebar.navigationItems;
      for (let i = 0; i < this.languages.length; i++) {
        this.languages[i] = ISO6391.getNativeName(this.languages[i]);
        this._translateService.onLangChange.subscribe((event: LangChangeEvent) => {
          window.location.reload();
        });
      }
    }
    this._profiles.supported().then((supportsProfiles) => {
      if (supportsProfiles) {
        this._profiles.getStudentProfiles().then((linkProfiles) => {
          linkProfiles.forEach((item) => {
            item.rel = item.profile === `urn:universis:student:profile:${student.id}` ? 'canonical' : 'alternate';
          });
          this.linkProfiles = linkProfiles;
        });
      }
    });
  }

  async getMessages() {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
    this.waitForMessages = true;
    this.notificationsMsgService.getUnreadMessages(this.take, this.skip).then(async (res) => {
      this.messages = res.value;
      this.waitForMessages = false;
    });
  }


  showDBAlertToast() {
    this._toastrService.warning(
      this._translateService.instant('DBSyncAlert.Message'),
      this._translateService.instant('DBSyncAlert.Title'),
    );
  }

  buildLanguages(url) {
    const findLang = this._appSidebar.navigationItems.find(x => {
      return x.url === '/lang';
    });
    if (findLang && findLang.children.length === 0) {

      const locales = this._configurationService.settings.i18n?.locales as string[];
      for (let i = 0, x = 0; i < locales.length; i++, x = x + 5) {
        this.lang = {};
        this.lang.name = ISO6391.getNativeName(locales[i]);
        this.lang.url = '/lang/'.concat(ISO6391.getCode(this.lang.name)).concat('/').concat(url);
        this.lang.icon = 'icon-cursor';
        this.lang.index = x;
        findLang.children.push(this.lang);
        if (locales.length < findLang.children.length) {
          findLang.children.splice(0, locales.length);
        }
      }
    }
  }


  changeLanguage(lang) {
    // auto-reloads on language change
    removeMemoizeKey(GradesService, 'getGradeInfo');
    removeMemoizeKey(GradesService, 'getLastExamPeriod');
    removeMemoizeKey(GradesService, 'getRecentGrades');
    sessionStorage.removeItem('availableClasses');
    this._configurationService.currentLocale = ISO6391.getCode(lang);
  }

  hideSidebar() {
    (document.querySelector('body') as HTMLBodyElement).classList.remove('sidebar-show');
  }

  select(linkProfile: LinkProfile): void {
    //change profiles
    this._profiles.setStudentProfile(linkProfile);
    window.location.reload();
  }

  toggleSidebarVisibility(): void {
    const body = (document.querySelector('body') as HTMLBodyElement);
    const isVisible = body.classList.contains('sidebar-show');
    if (isVisible) {
      body.classList.remove('sidebar-show');
      return;
    }
    body.classList.add('sidebar-show');
  }

}
