export const STUDENTS_APP_LOCATIONS = [
    {
        'privilege': 'Location',
        'help': 'Enables student registration feature',
        'target': {
            'url': '^/registrations'
        },
        'mask': 0
    },
    {
        'privilege': 'Location',
        'help': 'Enables student progress feature',
        'target': {
            'url': '^/students/progress'
        },
        'mask': 0
    },
    {
        'privilege': 'Location',
        'target': {
            'url': '^/auth/'
        },
        'mask': 1
    },
    {
        'privilege': 'Location',
        'target': {
            'url': '^/error'
        },
        'mask': 1
    },
    {
        'privilege': 'Location',
        'account': {
            'name': 'Students'
        },
        'target': {
            'url': '^/'
        },
        'mask': 1
    }
];
